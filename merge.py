#!/usr/bin/env python

import sys
from marshal import load, dump

from argparse import ArgumentParser
from os.path import exists


def updatebad(f_in, badconns, hostsindex):
    sys.stderr.write('Loading input bad data: ')
    with open(f_in, 'r') as f:
        data = load(f)
    sys.stderr.write("done.\n")
    sys.stderr.write('Adding input bad data: ')
    for i in data.iterkeys():
        host1 = hostsindex[i]
        if not host1 in badconns:
            badconns[host1] = dict()
        for j in data[i].iterkeys():
            host2 = hostsindex[j]
            if not host2 in badconns[host1]:
                badconns[host1][host2] = 1
            else:
                badconns[host1][host2] += 1
    sys.stderr.write("done.\n")
    return badconns


def updatehosts(f_in, nodes):
    sys.stderr.write('Loading input hosts data: ')
    with open(f_in, 'r') as f:
        data = load(f)
    sys.stderr.write("done.\n")
    sys.stderr.write('Adding input hosts data: ')
    for host in data:
        if host not in nodes:
            nodes.append(host)
    sys.stderr.write("done.\n")
    return nodes


def updategood(f_in, goodconns, hostsindex):
    sys.stderr.write('Loading input good data: ')
    with open(f_in, 'r') as f:
        data = load(f)
    sys.stderr.write("done.\n")
    sys.stderr.write('Adding input good data: ')
    for i in data.iterkeys():
        host1 = hostsindex[i]
        if host1 not in goodconns:
            goodconns[host1] = set()
        for j in data[i]:
            host2 = hostsindex[j]
            if host2 not in goodconns[host1]:
                goodconns[host1].add(host2)
    sys.stderr.write("done.\n")
    return goodconns


if __name__ == "__main__":
    parser = ArgumentParser(description="Merge data and improve structure.")
    parser.add_argument("--status", type=str, help="Status file prefix.")
    parser.add_argument("--input", type=str, help="Input file prefix.")
    parser.add_argument("--output", type=str, help="Output file prefix.")
    args = parser.parse_args()

    if args.status:
        assert exists(args.status+'good'), 'Non-existant status file: %s.' % args.status+'good'
        assert exists(args.status+'bad'), 'Non-existant status file: %s.' % args.status+'bad'
        assert exists(args.status+'hosts'), 'Non-existant status file: %s.' % args.status+'hosts'
    assert args.input, 'No input file prefix.'
    assert exists(args.input+'good'), 'Non-existant input file: %s.' % args.input+'good'
    assert exists(args.input+'bad'), 'Non-existant input file: %s.' % args.input+'bad'
    assert exists(args.input+'hosts'), 'Non-existant input file: %s.' % args.input+'hosts'
    assert args.output and not exists(args.output), 'Invalid output file.'

    if not args.status:
        hosts = []
    else:
        sys.stderr.write('Loading status host data: ')
        with open(args.status+'hosts', 'r') as f:
            hosts = load(f)
        del f
        sys.stderr.write("done.\n")

    hosts = updatehosts(args.input+'hosts', hosts)
    sys.stderr.write('Dumping host data: ')
    with open(args.output+'hosts', 'w') as f:
        dump(hosts, f)
    sys.stderr.write("done.\n")

    sys.stderr.write('Generating host index: ')
    hostsindex = dict()
    for host in hosts:
        hostsindex[host] = hosts.index(host)
    del hosts
    sys.stderr.write("done.\n")

    if not args.status:
        badconns = dict()
    else:
        sys.stderr.write('Loading status bad data: ')
        with open(args.status+'bad', 'r') as f:
            badconns = load(f)
        sys.stderr.write("done.\n")
    badconns = updatebad(args.input+'bad', badconns, hostsindex)

    sys.stderr.write('Dumping bad data: ')
    with open(args.output+'bad', 'w') as f:
        dump(badconns, f)
    del badconns
    sys.stderr.write("done.\n")

    if not args.status:
        goodconns = dict()
    else:
        sys.stderr.write('Loading status good data: ')
        with open(args.status+'good', 'r') as f:
            goodconns = load(f)
        sys.stderr.write("done.\n")
    goodconns = updategood(args.input+'good', goodconns, hostsindex)
    del hostsindex

    sys.stderr.write('Dumping good data: ')
    with open(args.output+'good', 'w') as f:
        dump(goodconns, f)
    del goodconns
    sys.stderr.write("done.\n")

