#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from argparse import ArgumentParser
from os.path import exists
from marshal import dump
from re import match


def parse_input(f_in):
    """
    Parse input file and return datastructures for relays, good connections, and bad connections.
    """
    relays = []
    # relaysset exists only to speed up lookups
    relaysset = set() 
    connsgood = dict()
    connsbad = dict()
    sys.stderr.write("Parsing input file %s: " % str(f_in))
    with open(f_in, 'r') as g:
        for line in g.xreadlines():
            if not line:
                break
            line = line.split(',')
            assert len(line) == 5, 'Invalid input length for %s' % \
                                    str(line)
            assert match('^[A-Z0-9]{40}$', line[0]), \
                         'Invalid host: %s' % str(line[0])
            assert match('^[A-Z0-9]{40}$', line[1]) or \
                   match('', line[1]), 'Invalid host: %s' % str(line[1])
            if line[0] not in relaysset:
                relaysset.add(line[0])
                relays.append(line[0])
            if match('^[A-Z0-9]{40}$', line[1]):
                if line[1] not in relaysset:
                    relaysset.add(line[1])
                    relays.append(line[1])
                if line[2] == 'REQUESTED' and line[3] == 'None':
                    if not line[0] in connsgood:
                        connsgood[line[0]] = set()
                    if not line[1] in connsgood[line[0]]:
                        connsgood[line[0]].add(line[1])
                else:
                    if not line[0] in connsbad:
                        connsbad[line[0]] = dict()
                    if not line[1] in connsbad[line[0]]:
                        connsbad[line[0]][line[1]] = 1
                    else:
                        connsbad[line[0]][line[1]] += 1
    sys.stderr.write("done.\n")
    return relays, connsgood, connsbad


if __name__ == '__main__':
    parser = ArgumentParser(description="FIXME")
    parser.add_argument("--input", type=str, help="Input file.")
    parser.add_argument("--output", type=str, help="Prefix for output files.")
    args = parser.parse_args()

    assert args.input, 'No input file.'
    assert args.output, 'No output file prefix.'
    assert exists(args.input), 'Non-existant input file: %s.' % args.input
    assert not exists(args.output+'hosts'), 'Output file already exists: %s%s.' % (args.output, 'hosts')
    assert not exists(args.output+'good'), 'Output file already exists: %s%s.' % (args.output, 'good')
    assert not exists(args.output+'bad'), 'Output file already exists: %s%s.' % (args.output, 'bad')

    hosts, goodconnections, badconnections = parse_input(args.input)

    sys.stderr.write("Dumping %d hosts: " % len(hosts))
    with open(args.output+'hosts', 'w') as f:
        dump(hosts, f)
    sys.stderr.write("done.\n")
    tmp = sum([len(goodconnections[i]) for i in goodconnections.iterkeys()])
    sys.stderr.write("Dumping %d good connections: " % tmp)
    with open(args.output+'good', 'w') as f:
        dump(goodconnections, f)
    sys.stderr.write("done.\n")
    tmp = 0
    for i in badconnections.iterkeys():
        for j in badconnections[i].iterkeys():
            tmp += badconnections[i][j]
    sys.stderr.write("Dumping %d bad connections: " % tmp)
    with open(args.output+'bad', 'w') as f:
        dump(badconnections, f)
    sys.stderr.write("done.\n")

