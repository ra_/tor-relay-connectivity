#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Generate Tor paths.
"""

import sys
from random import shuffle
from itertools import product
from argparse import ArgumentParser
from os.path import exists
from datetime import datetime
from marshal import load

from stem.connection import connect_port
from stem import InvalidArguments


def conn_bad(conn, conns_good, hostsindex):
    """ Check if a connection has never been reported working. """
    if not conn[0] in hostsindex or not conn[1] in hostsindex:
        return True
    if not hostsindex[conn[0]] in conns_good:
        return True
    if not hostsindex[conn[1]] in conns_good[hostsindex[conn[0]]]:
        return True
    return False


def relay_known(relay, controller):
    """
    Check if we know the network status and have a server descriptor for relay.
    """
    try:
        controller.get_network_status(relay)
        controller.get_server_descriptor(relay)
    except InvalidArguments:
        return False
    return True
    

def is_no_dup(conn):
    """ Check if both hosts of a connection are different. """
    if conn[0] != conn[1]:
        return True
    return False


if __name__ == '__main__':
    parser = ArgumentParser(description="Generate paths.")
    parser.add_argument("--input", type=str, help="Input file prefix.")
    parser.add_argument("--output", type=str, help="Output file.")
    args = parser.parse_args()

    if args.input:
        assert exists(args.input+'hosts'), 'Invalid input file: %s.' % args.input+'hosts'
        assert exists(args.input+'good'), 'Invalid input file: %s.' % args.input+'good'
    assert args.output and not exists(args.output), 'Invalid output file.'

    hosts = []
    goodconnections = dict()
    hostsindex = dict()
    if args.input:
        sys.stderr.write('Loading host data: ')
        with open(args.input+'hosts', 'r') as f:
            hosts = load(f)
        del f
        sys.stderr.write("done.\n")

        sys.stderr.write('Generating host index: ')
        hostsindex = dict()
        for host in hosts:
            hostsindex[host] = hosts.index(host)
        sys.stderr.write("done.\n")

        sys.stderr.write('Loading good data: ')
        with open(args.input+'good', 'r') as f:
            goodconnections = load(f)
        del f
        sys.stderr.write("done.\n")

    controller = connect_port()
    if len(hosts) == 0:
        sys.stderr.write("Fetching live hosts: ")
        hosts = [i.fingerprint for i in controller.get_network_statuses() if (datetime.now() - i.published).days == 0]
    else:
        sys.stderr.write("Of those %d hosts - live hosts: " % len(hosts))
        hosts = filter(lambda x: relay_known(x, controller), hosts)
    sys.stderr.write("%d\n" % len(hosts))
    controller.close()
    del controller

    # Build cartesian product of hosts list.
    sys.stderr.write("Building new path list: ")
    paths = list(product(hosts, hosts))
    del hosts
    sys.stderr.write("%d\n" % len(paths))

    if len(goodconnections) > 0:
        sys.stderr.write("Removing good connections from path list: ")
        paths = filter(lambda x: conn_bad(x, goodconnections, hostsindex), paths)
        sys.stderr.write("%d.\n" % len(paths))
    del goodconnections

    sys.stderr.write("Removing paths with identical hosts from list: ")
    paths = filter(is_no_dup, paths)
    sys.stderr.write("%d.\n" % len(paths))

    sys.stderr.write("Shuffle path list: ")
    shuffle(paths)
    sys.stderr.write("%d\n" % len(paths))

    sys.stderr.write("Writing path list to file: ")
    with open(args.output, 'w') as f_out:
        for i in paths:
            f_out.write("%s,%s\n" % (i[0], i[1]))
    sys.stderr.write("done.\n")
