#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Build Tor circuits from paths.
"""

# Author: ra <r.a@posteo.net>
# License: GPLv2 (2014)

import sys
from threading import Thread, Lock, Event, stack_size
from collections import namedtuple
from argparse import ArgumentParser
from os.path import exists
from re import match

from stem import InvalidRequest, InvalidArguments
from stem.control import EventType
from stem.connection import connect_port


Probe = namedtuple('Probe', 'path circs')

# Minimum stack size to possibly allow more concurrent worker threads.
stack_size(32768)


class _Manager(Thread):
    """
    Start worker threads and provide methods to them for accessing shared
    resources exclusively.
    """
    def __init__(self, controller, num_threads, f_in, f_out):
        self._controller = controller
        self._lock = Lock()
        self._f_in = f_in
        self._f_out = open(f_out, 'w')
        self._circ_closed = Event()
        self._nodes_processing = set()
        self._paths_waiting = []
        self.circ_launched = Lock()
        self._threads = set()
        self._threads_finished = []
        self._num_threads = num_threads
        self._worker_finished = Event()
        self._worker_finished.set()
        self._paths = self._pathsgen()
        Thread.__init__(self)
        self.start()

    def _start_worker(self, path):
        """ Start worker thread. """
        thread = _Worker(self._controller, self, path)
        self._threads.add(thread)

    def run(self):
        while True:
            with self._lock:
                for _ in range(self._num_threads - len(self._threads)):
                    # Prefer any usable waiting path.
                    path = self._get_waiting_path()
                    if path:
                        self._start_worker(path)
                    # Respect queue size.
                    elif len(self._paths_waiting) >= 2 * self._num_threads:
                        break
                    # Find a new path and check if it can be used.
                    else:
                        try:
                            path = self._paths.next()
                        except StopIteration:
                            break
                        if self._is_unused(path):
                            for node in path:
                                assert not node in \
                                    self._nodes_processing, \
                                    '%s being processed.' % node
                                self._nodes_processing.add(node)
                            self._start_worker(path)
                        else:
                            self._paths_waiting.append(path)
                self._worker_finished.clear()

            sys.stderr.write('Threads: %d, ' % len(self._threads) +
                             'Queue: %d\n' % len(self._paths_waiting))
            # Stop Manager, if no new workers have been spawned and queue is
            # empty.
            if len(self._threads) == 0:
                break

            # Wait for a worker to signal that it finished
            self._worker_finished.wait()
            # Wait for that worker to really have finished and
            # prevent possible race condition.
            self._threads_finished[0].join()

            # Clean up thread queue.
            tmp = self._threads.copy()
            for _ in range(len(tmp)):
                thread = tmp.pop()
                if not thread.isAlive():
                    # Remove nodes from processing list.
                    for node in thread.path:
                        assert node in self._nodes_processing, \
                            'Node %s not in list.' % node
                        self._nodes_processing.remove(node)
                    self._threads.remove(thread)
                    self._threads_finished.remove(thread)
                    del thread
            del tmp

    def _get_waiting_path(self):
        """
        Choose a path that is currently in the waiting queue.
        """
        for path in self._paths_waiting:
            if self._is_unused(path):
                for node in path:
                    assert not node in self._nodes_processing, \
                        '%s being processed.' % node
                    self._nodes_processing.add(node)
                self._paths_waiting.remove(path)
                return path

    def _pathsgen(self):
        """
        Path generator from input file.
        """
        with open(self._f_in, 'r') as f:
            for line in f.xreadlines():
                if not line:
                    raise StopIteration
                line = line.split(',')
                assert len(line) == 2, 'Invalid input length for %s' % \
                                        str(line)
                line[0] = line[0].rstrip("\n")
                line[1] = line[1].rstrip("\n")
                assert match('^[A-Z0-9]{40}$', line[0]), \
                            'Invalid host: %s' % str(line[0])
                assert match('^[A-Z0-9]{40}$', line[1]), \
                            'Invalid host: %s' % str(line[1])
                assert line[0] != line[1], 'Invalid path: %s' % str(line)
                yield line

    def _is_unused(self, path):
        """
        A path is only usable at that time if all nodes within that
        path are currently not being probed.
        """
        for node in path:
            if node in self._nodes_processing:
                return False
        return True

    def write(self, worker, probe):
        """
        Write probe data exclusively to stdout.
        """
        with self._lock:
            if len(probe.circs) > 0:
                first = probe.path[0]
                second = ''
                if probe.circs[0].status == 'LAUNCHED' and \
                   probe.circs[1].status == 'EXTENDED':
                    second = probe.path[1]
                reason = probe.circs[len(probe.circs) - 1].reason
                remote_reason = probe.circs[len(probe.circs) - 1].remote_reason
                date = str(probe.circs[0].created)
                self._f_out.write("%s,%s,%s,%s,%s\n" % (first, second, reason,
                                                        remote_reason, date))
            self._threads_finished.append(worker)
            self._worker_finished.set()


class _Worker(Thread):
    """
    Thread that actually builds the circuit.
        "controller": an authenticated Tor controller.
        "manager": for accessing shared resources.
        "path": path to probe.
    """
    def __init__(self, controller, manager, path):
        self._controller = controller
        self._manager = manager
        self.path = path
        self._cid = None
        self._circuit_finished = Event()
        self._circuit_built = Event()
        Thread.__init__(self)
        self.start()

    def run(self):
        def _circuit_handler(event):
            """ Event handler for handling circuit states. """
            if not event.build_flags or 'IS_INTERNAL' not in event.build_flags:
                if event.id == self._cid:
                    probe.circs.append(event)
                    if self._circuit_built.is_set():
                        if event.status in ('FAILED', 'CLOSED'):
                            self._circuit_finished.set()
                    if not self._circuit_built.is_set():
                        if event.status in ('FAILED', 'BUILT'):
                            self._circuit_built.set()
                elif event.status == 'LAUNCHED' and not self._cid:
                    self._cid = event.id
                    probe.circs.append(event)
                    self._manager.circ_launched.release()

        probe = Probe(path=self.path, circs=[])

        # Build new circuit.
        # Launching a circuit must be exclusive since we get the circuit
        # identifier from the LAUNCH event.
        self._manager.circ_launched.acquire()
        self._controller.add_event_listener(_circuit_handler, EventType.CIRC)
        try:
            self._controller.extend_circuit(path=self.path)
        # Ignore unknown relays.
        except InvalidRequest:
            self._controller.remove_event_listener(_circuit_handler)
            self._manager.circ_launched.release()
            self._manager.write(self, probe)
            return
        self._circuit_built.wait()
        build_status = probe.circs[len(probe.circs) - 1].status
        assert build_status in ('BUILT', 'FAILED', 'CLOSED'), \
                                'Wrong circuit status: %s.' % build_status
        if build_status == 'FAILED':
            self._controller.remove_event_listener(_circuit_handler)
            self._manager.write(self, probe)
            return

        # Close circuit, but ignore if it does not exist anymore.
        try:
            self._controller.close_circuit(self._cid)
        except InvalidArguments:
            pass

        # Make sure circuit has finished.
        self._circuit_finished.wait()
        self._controller.remove_event_listener(_circuit_handler)

        # Output probe data.
        self._manager.write(self, probe)


def _main():
    """ Build Tor circuits. """
    parser = ArgumentParser(description="Build Tor circuits.")
    parser.add_argument("--input", type=str, help="Input file.")
    parser.add_argument("--output", type=str, help="Output file.")
    parser.add_argument("--port", type=int, default=9051, help="Tor control port.")
    parser.add_argument("--threads", type=int, default=1,
                        help="Maximum number of concurrent worker threads.")
    args = parser.parse_args()

    assert args.input and exists(args.input), 'Invalid input file.'
    assert args.output and not exists(args.output), 'Invalid output file.'

    controller = connect_port(port=args.port)
    if not controller:
        sys.stderr.write("ERROR: Couldn't connect to Tor.\n")
        sys.exit(1)
    if not controller.is_authenticated():
        controller.authenticate()

    try:
        # Configure Tor client
        controller.set_conf("__DisablePredictedCircuits", "1")
        controller.set_conf("LearnCircuitBuildTimeout", "0")
        controller.set_conf("CircuitBuildTimeout", "120")
        controller.set_conf("MaxClientCircuitsPending", "1024")

        # Close all non-internal circuits.
        for circ in controller.get_circuits():
            if not circ.build_flags or 'IS_INTERNAL' not in circ.build_flags:
                controller.close_circuit(circ.id)

        manager = _Manager(controller, args.threads, args.input, args.output)
        while True:
            manager.join(1)
            if not manager.is_alive():
                break

    except KeyboardInterrupt:
        pass

    finally:
        controller.reset_conf("__DisablePredictedCircuits")
        controller.reset_conf("CircuitBuildTimeout")
        controller.reset_conf("LearnCircuitBuildTimeout")
        controller.reset_conf("MaxClientCircuitsPending")
        controller.close()


if __name__ == "__main__":
    _main()
