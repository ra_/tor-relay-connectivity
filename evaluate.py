#!/usr/bin/env python

import sys
from marshal import load
from collections import namedtuple
from operator import itemgetter
from argparse import ArgumentParser
from os.path import exists


parser = ArgumentParser(description="Data evaluation.")
parser.add_argument("--input", type=str, help="Input file prefix.")
args = parser.parse_args()

assert args.input, 'No input file prefix.'
assert exists(args.input+'good'), 'Non-existant input file: %s.' % args.input+'good'
assert exists(args.input+'bad'), 'Non-existant input file: %s.' % args.input+'bad'
assert exists(args.input+'hosts'), 'Non-existant input file: %s.' % args.input+'hosts'

sys.stderr.write('Loading host data: ')
with open(args.input+'hosts', 'r') as f:
    hosts = load(f)
del f
sys.stderr.write("done.\n")

sys.stderr.write('Loading bad data: ')
with open(args.input+'bad', 'r') as f:
    bad = load(f)
del f
sys.stderr.write("done.\n")

sys.stderr.write('Converting to inbound bad data: ')
badinbound = dict()
for i in bad.iterkeys():
    for j in bad[i].iterkeys():
        tmp = bad[i][j]
        if j not in badinbound:
            badinbound[j] = dict()
        if i not in badinbound[j]:
            badinbound[j][i] = tmp
        else:
            badinbound[j][i] += tmp
sys.stderr.write("done.\n")

sys.stderr.write('Loading good data: ')
with open(args.input+'good', 'r') as f:
    good = load(f)
del f
sys.stderr.write("done.\n")

sys.stderr.write('Converting to inbound good data: ')
goodinbound = dict()
for i in good.iterkeys():
    for j in good[i]:
        if j not in goodinbound:
            goodinbound[j] = set()
        if i not in goodinbound[j]:
            goodinbound[j].add(i)
sys.stderr.write("done.\n")

sys.stderr.write('Adding bad data: ')
conns = dict()
for i in bad.iterkeys():
    host = hosts[i]
    for j in bad[i].iterkeys():
        tmp = bad[i][j]
        # do not count connections that were tested successfully
        if i in good and j in good[i]:
            if host not in conns:
                conns[host] = [0, 0, 0, tmp]
            else:
                conns[host][3] += tmp
            continue
        # minimum number of bad measurements
        if tmp >= 4:
            if host not in conns:
                conns[host] = [0, 0, 1, tmp]
            else:
                conns[host][2] += 1
                conns[host][3] += tmp
        else:
            if host not in conns:
                conns[host] = [0, 1, 0, tmp]
            else:
                conns[host][1] += 1
                conns[host][3] += tmp
sys.stderr.write("done.\n")

sys.stderr.write('Adding good data: ')
for i in good.iterkeys():
    host = hosts[i]
    tmp = len(good[i])
    if not host in conns:
        conns[host] = [tmp, 0, 0, tmp]
    else:
        conns[host][0] += tmp
        conns[host][3] += tmp
sys.stderr.write("done.\n")



sys.stderr.write('Filtering data: ')
Node = namedtuple('Node', 'idhex goodconn suspconn badconn total')
nodes = []
for conn in conns.iterkeys():
    total = conns[conn][3]
    node = Node(idhex=conn, goodconn=conns[conn][0], suspconn=conns[conn][1], badconn=conns[conn][2], total=total)
    nodes.append(node)
nodes = sorted(nodes, key=itemgetter(3, 2, 4), reverse=True)
badoutboundhosts = [hosts.index(i.idhex) for i in nodes[:500]]
del nodes
del conns
sys.stderr.write("done.\n")


sys.stderr.write('Adding bad data: ')
badbak = bad
bad = badinbound
conns = dict()
for i in bad.iterkeys():
    host = hosts[i]
    for j in bad[i].iterkeys():
        tmp = bad[i][j]
        # do not count connections that were tested successfully
        if i in good and j in good[i]:
            if host not in conns:
                conns[host] = [0, 0, 0, tmp]
            else:
                conns[host][3] += tmp
            continue
        # minimum number of bad measurements
        if tmp >= 4:
            if host not in conns:
                conns[host] = [0, 0, 1, tmp]
            else:
                conns[host][2] += 1
                conns[host][3] += tmp
        else:
            if host not in conns:
                conns[host] = [0, 1, 0, tmp]
            else:
                conns[host][1] += 1
                conns[host][3] += tmp
del bad
bad = badbak
del badbak
sys.stderr.write("done.\n")

sys.stderr.write('Adding good data: ')
goodbak = good
good = goodinbound
for i in good.iterkeys():
    host = hosts[i]
    tmp = len(good[i])
    if not host in conns:
        conns[host] = [tmp, 0, 0, tmp]
    else:
        conns[host][0] += tmp
        conns[host][3] += tmp
del good
good = goodbak
del goodbak
sys.stderr.write("done.\n")

sys.stderr.write('Filtering data: ')
Node = namedtuple('Node', 'idhex goodconn suspconn badconn total')
nodes = []
for conn in conns.iterkeys():
    total = conns[conn][3]
    node = Node(idhex=conn, goodconn=conns[conn][0], suspconn=conns[conn][1], badconn=conns[conn][2], total=total)
    nodes.append(node)

nodes = sorted(nodes, key=itemgetter(3, 2, 4), reverse=True)
badinboundhosts = [hosts.index(i.idhex) for i in nodes[:500]]
del nodes
del conns
sys.stderr.write("done.\n")




sys.stderr.write('Adding bad data: ')
conns = dict()
for i in bad.iterkeys():
    host = hosts[i]
    for j in bad[i].iterkeys():
        if j in badinboundhosts:
            continue
        tmp = bad[i][j]
        # do not count connections that were tested successfully
        if i in good and j in good[i]:
            if host not in conns:
                conns[host] = [0, 0, 0, tmp]
            else:
                conns[host][3] += tmp
            continue
        # minimum number of bad measurements
        if tmp >= 3:
            if host not in conns:
                conns[host] = [0, 0, 1, tmp]
            else:
                conns[host][2] += 1
                conns[host][3] += tmp
        else:
            if host not in conns:
                conns[host] = [0, 1, 0, tmp]
            else:
                conns[host][1] += 1
                conns[host][3] += tmp
sys.stderr.write("done.\n")

sys.stderr.write('Adding good data: ')
for i in good.iterkeys():
    host = hosts[i]
    tmp = len([j for j in good[i] if j not in badinboundhosts])
    if not host in conns:
        conns[host] = [tmp, 0, 0, tmp]
    else:
        conns[host][0] += tmp
        conns[host][3] += tmp
sys.stderr.write("done.\n")

sys.stderr.write('Filtering data: ')
Node = namedtuple('Node', 'idhex goodconn suspconn badconn total')

nodes = []
for conn in conns.iterkeys():
    total = conns[conn][3]
    node = Node(idhex=conn, goodconn=conns[conn][0], suspconn=conns[conn][1], badconn=conns[conn][2], total=total)
    nodes.append(node)

nodes = sorted(nodes, key=itemgetter(3, 2, 4), reverse=True)

print "IDHEX,GOODCONNECTIONS,SUSPICIOUSCONNECTIONS,BADCONNECTIONS,CONNECTIONTESTS"
for node in nodes:
    print "%s,%d,%d,%d,%d" % (node.idhex, node.goodconn, node.suspconn, node.badconn, node.total)
sys.stderr.write("done.\n")



